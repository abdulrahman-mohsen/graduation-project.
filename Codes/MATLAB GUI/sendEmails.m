function [] = sendEmails(destinationEmailAddress) 
% User input
source = 'epilepsyprediction@gmail.com';              %from address (gmail)
%destination = 'reem27797@gmail.com';              %to address (any mail service)
%destination = textscan(destination, '%s', 'Delimiter',',');
%destination = destination{:};
myEmailPassword = 'epilepsycompany';                  %the password to the 'from' account
subj = 'Pending Seizure Alert';  % subject line
msg = 'Upcoming Seizure!';     % main body of email.
%set up SMTP service for Gmail
setpref('Internet','E_mail',source);
setpref('Internet','SMTP_Server','smtp.gmail.com');
setpref('Internet','SMTP_Username',source);
setpref('Internet','SMTP_Password',myEmailPassword);
% Gmail server.
props = java.lang.System.getProperties;
props.setProperty('mail.smtp.auth','true');
props.setProperty('mail.smtp.starttls.enable','true');
props.setProperty('mail.smtp.socketFactory.class', 'javax.net.ssl.SSLSocketFactory');
props.setProperty('mail.smtp.socketFactory.port','465');
% Send the email
 sendmail(destinationEmailAddress,subj,msg);
% [Optional] Remove the preferences (for privacy reasons)
setpref('Internet','E_mail','');
setpref('Internet','SMTP_Server','''');
setpref('Internet','SMTP_Username','');
setpref('Internet','SMTP_Password','');
