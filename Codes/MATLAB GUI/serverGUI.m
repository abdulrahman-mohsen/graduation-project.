function varargout = serverGUI(varargin)
% SERVERGUI MATLAB code for serverGUI.fig
%      SERVERGUI, by itself, creates a new SERVERGUI or raises the existing
%      singleton*.
%
%      H = SERVERGUI returns the handle to a new SERVERGUI or the handle to
%      the existing singleton*.
%
%      SERVERGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SERVERGUI.M with the given input arguments.
%
%      SERVERGUI('Property','Value',...) creates a new SERVERGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before serverGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to serverGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help serverGUI

% Last Modified by GUIDE v2.5 04-Aug-2020 12:01:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @serverGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @serverGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before serverGUI is made visible.
function serverGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to serverGUI (see VARARGIN)

% Choose default command line output for serverGUI
disp('opening function');
handles.output = hObject;
set(handles.message_display, 'String', 'Press Run');

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes serverGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = serverGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
disp('output function');
varargout{1} = handles.output;
%handles.Data= 0;
global Data;
varargout{2} = Data;
guidata(hObject, handles);




% --- Executes on selection change in records_menu.
function records_menu_Callback(hObject, eventdata, handles)
% hObject    handle to records_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns records_menu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from records_menu

str = cellstr(get(hObject,'String'));
pop_choice = str(get(hObject,'Value'));

if(strcmp(pop_choice, 'record1'))
    data = load('E:\Odious hole\Major\Graduation Project\GP\GP_IoT\Data\preprocessed_norm_Data.mat'); %load record1
    handles.Data = data.Data; %extract the matrix from the structure
elseif (strcmp(pop_choice, 'record2'))
    data = load('E:\Odious hole\Major\Graduation Project\GP\GP_IoT\Data\preprocessed_norm_Data_2.mat'); %load record2
    handles.Data = data.Data; %extract the matrix from the structure
elseif (strcmp(pop_choice, 'record3'))
    data = load('E:\Odious hole\Major\Graduation Project\GP\GP_IoT\Data\preprocessed_norm_Data_3.mat'); %load record3
    handles.Data = data.Data; %extract the matrix from the structure
elseif (strcmp(pop_choice, 'record4'))
    data = load('E:\Odious hole\Major\Graduation Project\GP\GP_IoT\Data\preprocessed_norm_Data_4.mat'); %load record4
    handles.Data = data.Data; %extract the matrix from the structure

end
    
disp(handles);
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function records_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to records_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in run_model.
function run_model_Callback(hObject, eventdata, handles)
% hObject    handle to run_model (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
guidata(hObject, handles);
%handles.email_field.String
%handles.run_model = runModel(handles.email_field.String);






disp('run model');
load('E:\Odious hole\Major\Graduation Project\GP\GP_IoT\trainedModel.mat'); %load the trained model
%disp(handles);

set(handles.message_display, 'String', 'processing');
guidata(hObject,handles);
drawnow
onesCounter = 0;
zerosCounter = 0;
messagesCounter = 0;
sent = 0;
notsent = 0;
total = [];
%set(handles.message_display, 'String', length(handles.Data));
guidata(hObject,handles);
for i = 1:length(handles.Data)
    
    %set(handles.message_display, 'String', i);
    %drawnow
    %guidata(hObject,handles);
    %disp(i);
    %pause(1);
    %disp('resume');
    
        
    result = svmclassify(svmmodel, handles.Data(i,:));
    total = [total result];
    
    
    if result == 1
        onesCounter = onesCounter+1;
    end
    if result == 0
        zerosCounter = zerosCounter+1;
    end
    fprintf('Window %d   ones %d   zeros %d\n', i, onesCounter, zerosCounter);
    if zerosCounter == 10
        onesCounter = 0;
        zerosCounter = 0;
    end
    
    if onesCounter == 20
        messagesCounter = messagesCounter + 1;
        sendEmails(handles.email_field.String);
        set(handles.message_display, 'String', sprintf('message %d sent', messagesCounter));
        disp(i);
        guidata(hObject,handles);
        drawnow
        disp('pause');
        pause(3);
        disp('resume');
       
        set(handles.message_display, 'String', 'Processing');
        guidata(hObject,handles);
        drawnow
        %disp('senttttttttttttttttttttttttttttttttttttttttt');
        sent = sent+1;
        
%         if zerosCounter == 10
%             onesCounter = 0;
%             zerosCounter = 0;
%             
%         end
    else
        %disp('waiting');
        notsent = notsent+1;
    end
%     if (onesCounter>10 && zerosCounter == 10)
%         onesCounter = 0;
%         zerosCounter = 0;
%     end
    
    
end %end for loop
set(handles.message_display, 'String', ' end');
drawnow
disp(sent);
disp(notsent);
guidata(hObject,handles);


%{
if (handles.run_model == 1)
    set(handles.message_display, 'String', 'message sent');
else
    set(handles.message_display, 'String', ' processing');
end
set(handles.message_display, 'String', 0);

%handles.message_display=handles.run_model;
%disp(handles.email_field.String);
%}

function email_field_Callback(hObject, eventdata, handles)
% hObject    handle to email_field (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of email_field as text
%        str2double(get(hObject,'String')) returns contents of email_field as a double
handles.email_field = get(hObject, 'String');
%guidata(hObject, handles);
%sendEmails(destimationEmailAddress);
%disp(destimationEmailAddress);


% --- Executes during object creation, after setting all properties.
function email_field_CreateFcn(hObject, eventdata, handles)
% hObject    handle to email_field (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function message_display_Callback(hObject, eventdata, handles)
% hObject    handle to message_display (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of message_display as text
%        str2double(get(hObject,'String')) returns contents of message_display as a double



% --- Executes during object creation, after setting all properties.
function message_display_CreateFcn(hObject, eventdata, handles)
% hObject    handle to message_display (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
