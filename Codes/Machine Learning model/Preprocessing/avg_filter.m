function [filtered_TS] = avg_filter(TS,window)
    filtered_TS=zeros(size(TS,1)-window+1,size(TS,2));
    for i=1:size(TS,1)-window+1
       filtered_TS(i)=sum(TS(i:i+window-1,1))/window; 
    end
end