%EDF to mat conversion - https://www.mathworks.com/matlabcentral/fileexchange/38641-reading-and-saving-of-data-in-the-edf
% 
% filePath_edfSource = 'D:\Graduation Project\Acodes\chb01_03.edf';
% filePath_matDestination = 'D:\Graduation Project\Acodes\chb01_03.edf';
% 
% [data, header] = readEDF(filePath_edfSource);
% data = cell2mat(data);
% save(filePath_matDestination, 'data');
% 
[data,header]=ReadEDF('D:\Graduation Project\Acodes\chb01_03.edf');
data=cell2mat(data);


save('chb01_033.mat','data');