
import 'package:flutter/material.dart';
import 'main.dart';
import 'Close.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart';
import 'globals.dart' as globals;


 // bool loop = false;
  var lastMessageID = '0';
  var lastDecMessageID = '0';
  bool skipFirstWarning = false;
  var messageBody='0';
  var warningMessageBody = '0';
  var access_token='0';
  var id='0';
  var patientEmail=' ';
  var patientName = ' ';
  var display = 'init';
  var displayName='0';
  var displayEmail='0';
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();


  final GoogleSignIn googleSignIn =new GoogleSignIn(
   scopes: [
    'email',
    //'https://www.googleapis.com/auth/contacts.readonly',
    'https://mail.google.com/',
   ],
  );
  final FirebaseAuth auth = FirebaseAuth.instance;

 _signOut(context) async{
   globals.loop = false;

      SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('email');
    prefs.remove('name');  
    prefs.remove('photo'); 
    prefs.remove('id'); 
    prefs.remove('accesstook');
    prefs.remove('type');
    prefs.remove('History');

 googleSignIn.signOut();
 auth.signOut();
 print('Signed out');
 Navigator.push(context,
    new MaterialPageRoute(
       builder: (context)=> new Close()),
    ); 
 }



void showNotification() async{
    var androidNotificationDetails = AndroidNotificationDetails('channel_id', 'channel_name', 'channel_description', importance: Importance.Max, priority: Priority.High);
    var iosNotificationDetails = IOSNotificationDetails();
    var notificationDetails = NotificationDetails(androidNotificationDetails, iosNotificationDetails);
    await flutterLocalNotificationsPlugin.show(0, 'Pending Seizure', '$warningMessageBody',notificationDetails);
  }


  void checkMail(Timer t) async{

    if (globals.loop == true || globals.loop == false){
      var declaration = await get(
       'https://www.googleapis.com/gmail/v1/users/$id/messages?maxResults=1&q=subject%3ADeclaration',
        headers: {
          'Authorization': 'Bearer ' + access_token
          }
       );
      print(declaration.body);
      Map declarationResp = convert.jsonDecode(declaration.body);
      if (declarationResp['resultSizeEstimate'] !=0 && declarationResp['messages'][0]['id'] != lastDecMessageID){
        //patientEmail = declarationResp['payload']['headers'][3]['value'];
        //print('patinetEmail: $patientEmail');
        lastDecMessageID = declarationResp['messages'][0]['id'];

        var messageContent = await get(
       'https://www.googleapis.com/gmail/v1/users/${id}/messages/'+declarationResp['messages'][0]['id'],
        headers: {
          'Authorization': 'Bearer ' + access_token
          }
       );

        Map messageContentDecoded = convert.jsonDecode(messageContent.body);
        print(messageContentDecoded);
        //setState(() {});
        patientEmail = messageContentDecoded['payload']['headers'][17]['value'];
        patientEmail = patientEmail.split('<')[1];
        patientEmail = patientEmail.split('>')[0];

        patientName = messageContentDecoded['payload']['headers'][17]['value'].split('<')[0];
        print('patinetEmail: $patientEmail');
        print('patientName: $patientName');

      }
      

      var resp = await get(
       'https://www.googleapis.com/gmail/v1/users/$id/messages?maxResults=1&q=subject%3A%20Pending%20Seizure%20Alert',
        headers: {
          'Authorization': 'Bearer ' + access_token
          }
       );
       
     print(resp.body); 


     Map mapRes = convert.jsonDecode(resp.body);
      //messageBody = mapRes['snippet'];
      //patientEmail = mapRes['payload']['headers'][4]['value'];
      
      if (skipFirstWarning == false){
        skipFirstWarning =true;
        lastMessageID =  mapRes['messages'][0]['id'];
        print('Ignore error message');
      }

      //the condition mapRes['resultSizeEstimate'] != 0, is to prevent tthe error of trying to access a field of a Null variable when there is no messages
     if (mapRes['resultSizeEstimate'] != 0 && mapRes['messages'][0]['id'] != lastMessageID){

       print('Fetching the message with ID: '+mapRes['messages'][0]['id']);
        var messageContent = await get(
       'https://www.googleapis.com/gmail/v1/users/${id}/messages/'+mapRes['messages'][0]['id'],
        headers: {
          'Authorization': 'Bearer ' + access_token
          }
       );

       //Retrieve message time and body
       print('Decoding fetched message...');
        Map messageContentDecoded = convert.jsonDecode(messageContent.body);
        //print(messageContentDecoded);
        print ('new message date');
        //print(messageContentDecoded['payload']['headers'][3]['value'][1]); //Message date
        warningMessageBody = messageContentDecoded['snippet'];


        //push notification
        showNotification();
        lastMessageID = mapRes['messages'][0]['id'];
        print('New message received');

        print('Extracting metadata from matlab message');
        lastMessageID = mapRes['messages'][0]['id'];
        //globals.messageDate[0] = messageContentDecoded['payload']['headers'][3]['value'];
        
        
        globals.messageDate.insert(0, DateTime.now().toString()); //insert the new date at the beginning of the list
        if (globals.messageDate.length > globals.N_datesHistory ){
          globals.messageDate.removeAt(globals.messageDate.length-1); //remove the last element 
        }
        print('History');
        print(globals.messageDate);
        globals.dateCounter= globals.dateCounter - 1;
        if (globals.dateCounter < 0){
          globals.dateCounter = globals.N_datesHistory-1;
        }

        //////  
     }

    }
    else{
      t.cancel();
    }
    
  }


_saveHistory() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList('History',globals.messageDate);
} 






class RelativeProfileScreen extends StatefulWidget {
    final SignDetails details;
    final List history; //edit

  RelativeProfileScreen({Key key,@required this.details,this.history}) : super(key: key);

  @override
  _RelativeProfileScreenState createState() => _RelativeProfileScreenState();
}

class _RelativeProfileScreenState extends State<RelativeProfileScreen> {
  
   @override
  void initState(){
  super.initState();
  access_token = widget.details.took;
  id = widget.details.uID;
  globals.messageDate=widget.history; 

  var androidSettings = AndroidInitializationSettings('@mipmap/ic_launcher');
  var iosSettings = IOSInitializationSettings();
  var initializationSettings = InitializationSettings(androidSettings,iosSettings);
  flutterLocalNotificationsPlugin.initialize(initializationSettings);

  
  }
    final GlobalKey<AnimatedListState> _key= GlobalKey();  ///key of Animated List
  @override
  
  Widget build(BuildContext context) {

    const interval = const Duration(seconds:10);
    new Timer.periodic(interval, checkMail);
    

     print('build Enter');
    double _width = MediaQuery.of(context).size.width/100;
    double _height = MediaQuery.of(context).size.height/100;
    print(_height);
    return new MaterialApp(
      title: 'Relative Profile',
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar:  AppBar(
          title:  Text( "Relative's Profile",
            style: TextStyle(color: Colors.black,fontSize: 20.0, fontFamily: 'Fredoka'),),
          automaticallyImplyLeading: true,
          backgroundColor:Colors.teal[100],
        ),

          body: Column(
           mainAxisAlignment: MainAxisAlignment.spaceBetween,
           children: <Widget>[
           Container( 
             height: 40* _height,
             width: 100*_width,
             //alignment: Alignment.center,
             decoration: BoxDecoration(
              borderRadius: BorderRadius.only(bottomLeft:Radius.circular(60),bottomRight:Radius.circular(60)) ,
               color: Colors.teal[100],
             ),
             child: Padding(
               padding: const EdgeInsets.only(left:20.0,  right:20.0,  top: 50.0),
               child: Column(
                 children:<Widget>[
                   Row(
                     children: <Widget>[
                        Container(
                         height: 11*_height ,
                         width: 22* _width,
                         decoration: BoxDecoration(
                           shape:BoxShape.circle,
                           image: DecorationImage(
                             fit: BoxFit.contain,
                             image: NetworkImage(widget.details.uPhoto))
                         ),
                       ),
                       SizedBox(width:5*_width),
                        Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: <Widget>[
                           Text(
                            "Name : " + widget.details.uName,
                              style:  TextStyle(color: Colors.black,fontSize: 15.0, fontFamily: 'Fredoka'),
                            ),
                            Text(
                              "User : Relative",
                              style:  TextStyle(color: Colors.black,fontSize: 15.0, fontFamily: 'Fredoka'),
                            ),
                            Text(
                              "E-mail : " + widget.details.uEmail,
                              //softWrap: ,
                              style:  TextStyle(color: Colors.black,fontSize: 14.0, fontFamily: 'Fredoka'),
                            ),
                         ],
                       ) 
                   ],),
                   SizedBox(height: 2*_height),
                   Column(
                     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                     children: <Widget>[
                       Container(
                          width: 200,
                          height: 30,
                          child: RaisedButton(
                            color: Colors.purpleAccent[50], ////why don'y work??????
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0)),
                             // padding: EdgeInsets.only(left: 40), //Shifting the text
                                child:Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                  Icon(FontAwesomeIcons.idCardAlt,color: Colors.deepPurple[300],),
                                  SizedBox(width: 10.0),
                                  Text( 'Your Patient Info',
                                    style: TextStyle(color: Colors.black,fontSize: 15.0, fontFamily: 'Fredoka'),)
                                ],
                                ),
                            onPressed: () {
                              setState((){displayEmail=patientEmail;});
                              setState((){displayName=patientName;});
                              showModalBottomSheet(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.only(topLeft: Radius.circular(40),topRight:Radius.circular(40)),
                                      ),
                                      context:context,builder: (cxt) =>  _patientInfo(cxt)
                                    );  
                           }
                          ),
                      ),
                      SizedBox(height: 3*_height),
                       Container(
                          width: 150,
                          height: 30,
                          child: RaisedButton(
                            color: Colors.purpleAccent[50], ////why don'y work??????
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0)),
                              //padding: EdgeInsets.only(left: 40), //Shifting the text
                                child:Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                  Icon(FontAwesomeIcons.signOutAlt,color: Colors.deepPurple[300],),
                                  SizedBox(width: 10.0),
                                  Text( 'Sign Out',
                                    style: TextStyle(color: Colors.black,fontSize: 15.0, fontFamily: 'Fredoka'),)
                                ],
                                ),
                            onPressed: () {
                              _signOut(context);
                           }
                          ),
                      ),
                     ],
                   )
                 ]
                 )
             )
             ),
              Container( 
             height: 7* _height,
             alignment: Alignment.bottomCenter,
             decoration: BoxDecoration(
              borderRadius: BorderRadius.only(topLeft:Radius.circular(50),topRight:Radius.circular(50)) ,
             ),
             child:RaisedButton(
                              color: Colors.teal[100], ////why don'y work??????
                              shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0)),
                              //padding: EdgeInsets.only(left: 30), //Shifting the text
                                  child:Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                  Icon(FontAwesomeIcons.addressCard,color: Colors.deepPurple[300],),
                                  SizedBox(width: 10.0),
                                  Text( "Seizures' History ",
                                  textAlign: TextAlign.right,
                                    style: TextStyle(color: Colors.black,fontSize: 15.0, fontFamily: 'Fredoka'),)
                                  ]
                                  ),
                                 onPressed: (){
                                     showModalBottomSheet(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.only(topLeft: Radius.circular(40),topRight:Radius.circular(40)),
                                      ),
                                      context:context,builder: (cxt) => _history(cxt)
                                    );  
                                    _saveHistory();
                                 },
                            )
             
             )])
           )); 
}

               

 _patientInfo(BuildContext context){
     return    Stack(    //Very Important ,It allow list with Button
        overflow: Overflow.visible,
        children: <Widget>[ 
        Container(
          alignment: Alignment.center,
          child:Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children:<Widget>[
              //Text('Patient Name: '+displayName ,
              //  style: TextStyle(color: Colors.black,fontSize: 23.0, fontFamily: 'Fredoka') ,),
              Text('Patient Email: '+displayEmail ,
                style: TextStyle(color: Colors.black,fontSize: 23.0, fontFamily: 'Fredoka') ,),

            ]
          
            ))
        ]);
} 


_history(BuildContext context){
   if(globals.messageDate.length==0){
     return    Stack(    //Very Important ,It allow list with Button
        overflow: Overflow.visible,
        children: <Widget>[ 
        Container(
          alignment: Alignment.center,
          child:Text('No History',
            style: TextStyle(color: Colors.black,fontSize: 35.0, fontFamily: 'Fredoka') ,))
        ]);
    }else{
   return   Stack(    //Very Important ,It allow list with Button
  overflow: Overflow.visible,
  children: <Widget>[
  AnimatedList(        //Use Animated List to easily add and delete items
    padding: EdgeInsets.only(top: 20,right: 20, left:20,bottom: 20),
             initialItemCount: globals.messageDate.length,
                  key: _key,
                  itemBuilder: ( context,  index , animation){
                   return _buildHistory(globals.messageDate[index],animation,index);
                  }
                ),
        ]);
    }
    }
 _buildHistory(String items, Animation animation ,int index) {
  return SizeTransition(
   sizeFactor:animation,
    child: Container(        // Actual widget to display
      height: 80.0,
      width: 0.9*(MediaQuery.of(context).size.width),
      child: Card(
        margin: EdgeInsets.all(8),
        elevation: 1.0,
        color: Colors.deepPurple[100],
        shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(30.0),),
        child: Center(
          child: ListTile(
            contentPadding: EdgeInsets.only(left:35),
            title:Text(items,style: TextStyle(color: Colors.black,fontSize: 15.0, fontFamily: 'Fredoka'),),
          ),  
      ),
    ),
  )
  );
  }
}

