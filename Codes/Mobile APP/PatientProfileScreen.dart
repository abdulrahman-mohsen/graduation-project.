import 'package:flutter/material.dart';
import 'main.dart';
import 'Close.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart';
import 'dart:async';

import 'globals.dart' as globals;


  bool loop = true;
  var lastMessageID = '0';
  bool skipFirstWarning = false;
  bool checkStop = false;
  var messageBody = '0';
  var messageSubject='0';
  var access_token = '0';
  var id = '0';
  var email = '0';
  //var relatives = '0';
  String rMailsString;
  List<String> _mails;


  

  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  

  final GoogleSignIn googleSignIn =new GoogleSignIn(
   scopes: [
    'email',
    //'https://www.googleapis.com/auth/contacts.readonly',
    'https://mail.google.com/',
   ],
  );
  final FirebaseAuth auth = FirebaseAuth.instance;

 _signOut(context) async{
   globals.loop = false;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('email');
    prefs.remove('name');  
    prefs.remove('photo'); 
    prefs.remove('id'); 
    prefs.remove('accesstook');
    prefs.remove('type');
    prefs.remove('R_emails');
    prefs.remove('History');

    googleSignIn.signOut();
    auth.signOut();
    print('Signed out');
    Navigator.push(context,
    new MaterialPageRoute(
       builder: (context)=> new Close()),
    ); 
  }


void sendRecognitionMail(String newRelativeEmail) async{
  //String message = "From: ${email}\r\n"+"To: ${newRelativeEmail}\r\n"+"Subject: Declaration\r\n\r\n"+"You are added as a relative";
  //List messageInLatin1 = convert.latin1.encode("From: ${email}\r\n"+"To: ${newRelativeEmail}\r\n"+"Subject: Declaration\r\n\r\n"+"You are added as a relative");
  var welcomeMessage = convert.base64.encode(convert.latin1.encode("From: $email\r\n"+"To: $newRelativeEmail\r\n"+"Subject: Declaration\r\n\r\n"+"You are added as a relative"));
  await post(
    'https://www.googleapis.com/gmail/v1/users/$id/messages/send',
      headers: {
        'Authorization': 'Bearer ' + access_token,
        'Content-Type': 'application/json; charset=UTF-8',
        },

      body: convert.jsonEncode(<String, String>{
        //'raw': '${base64From}${base64Subject}${base64To}'
        'raw': welcomeMessage
       // "raw":"[{'from': '$email', 'To': '$relatives', 'Subject': $subject}]"
      }),
                  
    ); 
    print('Informing the relative....$newRelativeEmail');
}



void sendEmails() async{
  rMailsString = _mails.join(',');
  rMailsString = rMailsString.replaceAll(' ', '');
  print('Patient constructs the warning email');
  String message = "From: $email\r\n"+"To: $rMailsString\r\n"+"Subject: Pending Seizure Alert\r\n\r\n"+"Patient requires immediate attention";
  List messageInLatin1 = convert.latin1.encode(message);
  var messageInBase64 = convert.base64.encode(messageInLatin1);

  print('Patient sends message to the relative');
  var response = await post(
    'https://www.googleapis.com/gmail/v1/users/$id/messages/send',
      headers: {
        'Authorization': 'Bearer ' + access_token,
        'Content-Type': 'application/json; charset=UTF-8',
        },

      body: convert.jsonEncode(<String, String>{
        //'raw': '${base64From}${base64Subject}${base64To}'
        'raw': messageInBase64
       // "raw":"[{'from': '$email', 'To': '$relatives', 'Subject': $subject}]"
      }),
                  
    );
  print('Patient done sending warning to relative');
  //print(response.body);
}



void showNotification() async{
  print('Sending Notification to the patient....');
    var androidNotificationDetails = AndroidNotificationDetails('channel_id', 'channel_name', 'channel_description', importance: Importance.Max, priority: Priority.High);
    var iosNotificationDetails = IOSNotificationDetails();
    var notificationDetails = NotificationDetails(androidNotificationDetails, iosNotificationDetails);
    await flutterLocalNotificationsPlugin.show(0, 'Pending Seizure', '$messageBody',notificationDetails);
  }



void checkMail(Timer t) async{
    print('Checking Patient MailBox');

   // print('Relatives: '+rMailsString);
    if (globals.loop == true || globals.loop == false){
      var resp = await get(
       'https://www.googleapis.com/gmail/v1/users/$id/messages?maxResults=1&q=from%3A%20epilepsyprediction%40gmail.com%20AND%20subject%3A%20Pending%20Seizure%20Alert',
        headers: {
          'Authorization': 'Bearer ' + access_token
          }
       );

     print('Found a message with that subject');  
     print(resp.body); 

     Map mapRes = convert.jsonDecode(resp.body);
     // messageBody = mapRes['snippet'];

      //print('message ID: '+mapRes['messages'][0]['id']);
      print('Last ID: '+lastMessageID);
      if (skipFirstWarning == false){
        skipFirstWarning =true;
        lastMessageID =  mapRes['messages'][0]['id'];
        print('Ignore error message');
      }

      //the condition mapRes['resultSizeEstimate'] != 0, is to prevent tthe error of trying to access a field of a Null variable when there is no messages
     if (mapRes['resultSizeEstimate'] != 0 && mapRes['messages'][0]['id'] != lastMessageID){
        //push notification
        print('Change in id: new '+mapRes['messages'][0]['id']+'vs. last ID '+lastMessageID);

        print('Fetching the message with ID: '+mapRes['messages'][0]['id']);
        var messageContent = await get(
       'https://www.googleapis.com/gmail/v1/users/$id/messages/'+mapRes['messages'][0]['id'],
        headers: {
          'Authorization': 'Bearer ' + access_token
          }
       );

       //Retrieve message time and body
       print('Decoding fetched message...');
        Map messageContentDecoded = convert.jsonDecode(messageContent.body);
        //print(messageContentDecoded);
        print ('new message date');
        //print(messageContentDecoded['payload']['headers'][3]['value'][1]); //Message date
        messageBody = messageContentDecoded['snippet'];

        showNotification();
        if(_mails.length!=0){
        sendEmails();
        }

        print('Extracting metadata from matlab message');
        lastMessageID = mapRes['messages'][0]['id'];
        //globals.messageDate[0] = messageContentDecoded['payload']['headers'][3]['value'];
        
        
        globals.messageDate.insert(0, DateTime.now().toString()); //insert the new date at the beginning of the list
        if (globals.messageDate.length > globals.N_datesHistory ){
          globals.messageDate.removeAt(globals.messageDate.length-1); //remove the last element 
        }
        print('History');
        print(globals.messageDate);
        globals.dateCounter= globals.dateCounter - 1;
        if (globals.dateCounter < 0){
          globals.dateCounter = globals.N_datesHistory-1;
          
        }
            
        //========//_saveHistory();     

        //print('New message received');
        //print(lastMessageID);
        
        
     }

      
    }///////////////////////////////////
    else{

      t.cancel();
    }
  }

_saveMails() async{
  if(_mails.length!=null){
  SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList('R_emails',_mails);
  }
}

_saveHistory() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList('History',globals.messageDate);
    
} 




class PatientProfileScreen extends StatefulWidget {
    final SignDetails details;
    final  List<String> mails; //edit
    List<String> history; //edit

    
    
  PatientProfileScreen({Key key,@required this.details, this.mails,this.history}) : super(key: key);

  @override
  _PatientProfileScreenState createState() => _PatientProfileScreenState();
}

class _PatientProfileScreenState extends State<PatientProfileScreen> {
  /*===============List Contain Relatives' mails==============*/

   @override
  void initState(){
  super.initState();
  access_token = widget.details.took;
  id = widget.details.uID;
  _mails= widget.mails;     //get relative emails of current patient
  globals.messageDate=widget.history;   //get history of current patient


  var androidSettings = AndroidInitializationSettings('@mipmap/ic_launcher');
  var iosSettings = IOSInitializationSettings();
  var initializationSettings = InitializationSettings(androidSettings,iosSettings);
  flutterLocalNotificationsPlugin.initialize(initializationSettings);
  }

  








   
  final GlobalKey<AnimatedListState> _key= GlobalKey();  ///key of Animated List
  
  @override
  Widget build(BuildContext context) {
    print('build Enter');
    
    //_saveHistory();
   
    const interval = const Duration(seconds:10);
    new Timer.periodic(interval, checkMail);

    


 
    print(rMailsString);
    double _width = MediaQuery.of(context).size.width/100;
    double _height = MediaQuery.of(context).size.height/100;
     return MaterialApp(
      title: 'Patient Profile',
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.brown[50],
        appBar:  AppBar(
          //leading: new Container(), //Removes the back button on the app bar
          title:  Text(
            "Patient's Profile",
            style:  TextStyle(color: Colors.black,fontSize: 20.0,),
            ),
          automaticallyImplyLeading: true,
          backgroundColor:Colors.deepPurple[100],
        ),
        body: Stack(  //stack allow be to overflow the containers on eachothers
         overflow: Overflow.visible,
          children: <Widget>[
           Container( 
             height: 40* _height,
             decoration: BoxDecoration(
              borderRadius: BorderRadius.only(bottomLeft:Radius.circular(60),bottomRight:Radius.circular(60)) ,
               color: Colors.deepPurple[100],
             ),

             /*===================Column of Name, MAil & photo==============*/
             child: Padding(
               padding: const EdgeInsets.only(left:30.0,  right:30.0,  top: 50.0),
               child: Column(
                 children:<Widget>[
                   Row(
                     children: <Widget>[
                       Container(
                         height: 11*_height ,
                         width: 22* _width,
                         decoration: BoxDecoration(
                           shape:BoxShape.circle,
                           image: DecorationImage(
                             fit: BoxFit.contain,
                             image: NetworkImage(widget.details.uPhoto))
                         ),
                       ),
                       SizedBox(width:5*_width),
                       Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: <Widget>[
                           Text(
                              "Name : " + widget.details.uName,
                              style:  TextStyle(color: Colors.black,fontSize: 15.0, fontFamily: 'Fredoka'),
                            ),
                            Text(
                              "User : Patient",
                              style:  TextStyle(color: Colors.black,fontSize: 15.0, fontFamily: 'Fredoka'),
                            ),
                            Text(
                              "E-mail : " + widget.details.uEmail,
                              //softWrap: ,
                              style:  TextStyle(color: Colors.black,fontSize: 14.0, fontFamily: 'Fredoka'),
                            ),
                         ],
                       )                       
                     ],
                   ),
                   SizedBox(height: 2*_height),
                   Row(
                     mainAxisAlignment: MainAxisAlignment.center,
                     children: <Widget>[
                       Container(
                          width: 160,
                          height: 30,
                          child: RaisedButton(
                            color: Colors.purpleAccent[50], ////why don'y work??????
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0)),
                                child:Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly, //make eaqualyspaces
                                  children: <Widget>[
                                  Icon(FontAwesomeIcons.signOutAlt,color: Colors.deepPurple[300],),
                                  Text( 'Sign Out',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.black,fontSize: 15.0, fontFamily: 'Fredoka'),)
                                ],
                                ),
                            onPressed: () {
                              _signOut(context);
                            }
                          ),
                      ),
                     ],
                   ),
                 ]
               ),
             ),
           ),

           /*===================the Bottom Containers of Relevants & History=========================*/ 
                    Container(
                      alignment: Alignment.bottomCenter,
                      child:Row(
                        //width: 200,
                         //height: 30, 
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly ,
                        crossAxisAlignment: CrossAxisAlignment.end,
                         children:<Widget>[                         
                             RaisedButton(
                              color: Colors.deepPurple[100], ////why don'y work??????
                              shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0)),
                              //padding: EdgeInsets.only(left: 30), //Shifting the text
                                  child:Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                  Icon(FontAwesomeIcons.users,color: Colors.deepPurple[300],),
                                  SizedBox(width: 10.0),
                                  Text( "Relatives' Emails",
                                  textAlign: TextAlign.right,
                                    style: TextStyle(color: Colors.black,fontSize: 15.0, fontFamily: 'Fredoka'),)
                                  ]
                                  ),
                                 onPressed: (){
                                    showModalBottomSheet(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.only(topLeft: Radius.circular(40),topRight:Radius.circular(40)),
                                      ),
                                      context:context,builder: (cxt) => _mailMenu(cxt)
                                    );
                                    _saveMails();
                                   print('received');
                                   print(widget.mails);
                                   print("receined _mails");
                                   print(_mails);
                                 },
                            ),
                            RaisedButton(
                              color: Colors.deepPurple[100], ////why don'y work??????
                              shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(50.0)),
                              //padding: EdgeInsets.only(left: 30), //Shifting the text
                                  child:Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                  Icon(FontAwesomeIcons.addressCard,color: Colors.deepPurple[300],),
                                  SizedBox(width: 10.0),
                                  Text( "Seizures' History ",
                                  textAlign: TextAlign.right,
                                    style: TextStyle(color: Colors.black,fontSize: 15.0, fontFamily: 'Fredoka'),)
                                  ]
                                  ),
                                 onPressed: (){
                                    showModalBottomSheet(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.only(topLeft: Radius.circular(40),topRight:Radius.circular(40)),
                                      ),
                                      context:context,builder: (cxt) => _history(cxt)
                                    );
                                    _saveHistory();
                                    print("Receive history");
                                    print(globals.messageDate);
                                 },
                            )
                         ]
                      ),
                    )
        ]
      )
     )
     );
  }

/*============Add Relative Buttom==============*/
addRelativeButton(){
return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children:<Widget>[
      Container(
          alignment:Alignment.bottomCenter,
          width: 250,
          child:RaisedButton(
             color: Colors.deepPurple[300],
             shape: RoundedRectangleBorder(
             borderRadius: new BorderRadius.circular(50.0)),
              child:Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                Icon(FontAwesomeIcons.userEdit,color: Colors.deepPurple[100],),
                Text( "Add New Relatives",
                textAlign: TextAlign.right,
                  style: TextStyle(color: Colors.black,fontSize: 15.0, fontFamily: 'Fredoka'),)
                ]
                ),
            onPressed: (){
               _addItem();
               _saveMails();
               
            }
          )
        ),]);
}
/*==============The Bottom sheet(Drawer)================ */
_mailMenu(BuildContext context){
  if(_mails.length==0){
     return   Stack(    //Very Important ,It allow list with Button
        overflow: Overflow.visible,
        children: <Widget>[
           Container(
            alignment: Alignment.center,
             child:Text('No Relative',
                    style: TextStyle(color: Colors.black,fontSize: 35.0, fontFamily: 'Fredoka'),
                    textAlign: TextAlign.center,),
                    ), 
                AnimatedList(        //Use Animated List to easily add and delete items
                 padding: EdgeInsets.only(top: 20,right: 20, left:20,bottom: 20),
                 initialItemCount: _mails.length,
                 key: _key,
                 itemBuilder: ( context,index,  animation){
                   return _buildItem(_mails[index],animation,0);
                 }
                ),
                    addRelativeButton()

     ]);
     
  }else{
     return   Stack(    //Very Important ,It allow list with Button
        overflow: Overflow.visible,
        children: <Widget>[
          AnimatedList(        //Use Animated List to easily add and delete items
                 padding: EdgeInsets.only(top: 20,right: 20, left:20,bottom: 20),
                 initialItemCount: _mails.length,
                 key: _key,
                 itemBuilder: ( context,index,  animation){
                   return _buildItem(_mails[index],animation,index);
                 }
                ),
                addRelativeButton()
                
        ]);
  }
}

/*===============Build the items of the List================ */
 _buildItem(String items , Animation animation,int index) {
   if(_mails.length==0){
    return Container(
      alignment: Alignment.center,
      child:Text('No Relatives',
        style: TextStyle(color: Colors.black,fontSize: 35.0, fontFamily: 'Fredoka'),
          textAlign: TextAlign.center,),
    );
  }else{
  return   Stack(    //Very Important ,It allow list with Button
  overflow: Overflow.visible,
  children: <Widget>[
    Container(        // Actual widget to display
      height: 80.0,
      width: 0.9*(MediaQuery.of(context).size.width),
      child: Card(
        margin: EdgeInsets.all(8),
        elevation: 1.0,
        color: Colors.deepPurple[100],
        shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(30.0),),
        child: Center(
          child: ListTile(
            contentPadding: EdgeInsets.only(left:35),
            title:Text(items,style: TextStyle(color: Colors.black,fontSize: 15.0, fontFamily: 'Fredoka'),),
           trailing: IconButton(
            icon: Icon(Icons.close, color:Colors.redAccent) ,
             onPressed: (){
               _alert(index);   //Confirm to Delete
             }
             )
          ),  
      ),
    ),
  )
  ]);
}}

/*==================Alert Message To Confirm Deleting==============*/
_alert(int index){
  showDialog<String>(           //the small square widget
    context:context,
    builder: (BuildContext context) =>AlertDialog(      //Making Allert Message
      title: const Text('Alert !',style: TextStyle(color: Colors.black,fontSize: 30.0, fontFamily: 'Fredoka')),
      content: Text('Are you sure you want to remove this email?',
      style: TextStyle(color: Colors.black,fontSize: 20.0, fontFamily: 'Fredoka')),
      actions: <Widget>[
        RaisedButton(
          child: Text('NO',style: TextStyle(color: Colors.black,fontSize: 15.0, fontFamily: 'Fredoka')),
          color: Colors.purple[100],
           shape: RoundedRectangleBorder(
             borderRadius: new BorderRadius.circular(50.0)),
          onPressed:()=>Navigator.pop(context),
        ),
        RaisedButton(
          child: Text('YES',style: TextStyle(color: Colors.black,fontSize: 15.0, fontFamily: 'Fredoka')),
          color: Colors.purple[100],
           shape: RoundedRectangleBorder(
             borderRadius: new BorderRadius.circular(50.0)),
          onPressed:(){
            //_mails.removeAt(index);
             _removeItem(index);
             _saveMails();
             print("mails after remove");
             print(_mails);
             Navigator.pop(context);
             if(_mails.isEmpty){
               Container(
                  alignment: Alignment.center,
                  child:Text('No Relatives',
                    style: TextStyle(color: Colors.black,fontSize: 35.0, fontFamily: 'Fredoka'),
                    textAlign: TextAlign.center,),
                );
              }
          }
        ),
      ]
    )
    );
}
/*============Initial Values needed for Add Items===========*/
// Must be outside the function after Build Widget
var _newEmail=""; 
TextEditingController _textController=  TextEditingController();

/// must use initState, setState and TextEditingControl to receive value from textfield and use it in button

/*===================Adding Items to Animated List========================== */
_addItem(){
  int i=_mails.length;
  print(i);
 _insert( var cxt){      //insert cxt yo the Mails List
     _mails.add(cxt);
     //AnimatedList.of(context).insertItem(i);
     _key.currentState.insertItem(i);
  }
  bool valid=false;
  

/*---------Opening a small square widget with text field to write new mail---------*/
showDialog<String>(
    context:context,
    builder: (BuildContext context) =>AlertDialog(
      title: const Text("Add The New Relative's E-mail",
          style: TextStyle(color: Colors.black,fontSize: 23.0, fontFamily: 'Fredoka')),
             content: TextFormField(
                      keyboardType:TextInputType.emailAddress,
                      controller: _textController, //used for initState
                      decoration: new InputDecoration(
                      hintText: 'eg. ****@gmail.com',
                      errorText: valid?  null : "Please Enter an E-mail" ,
                      labelStyle: TextStyle(color: Colors.black,fontSize: 30.0, fontFamily: 'Fredoka'),
                      suffixIcon: IconButton(icon: Icon(Icons.clear), onPressed:()=> _textController.clear())
                      ),
                      onChanged: (val){
                        _newEmail=val;                     
                      },
                      onFieldSubmitted: (val){  
                        final v= _newEmail.contains('@gmail.com');    //on submitt >>>if he press enter on keyboard   
                         print('Emal submit');
                          print(_newEmail); //check if the user inter email or not
                        if(v==true){
                          _insert(_newEmail);
                          _saveMails();
                          print("mails after insert");
                          print(_mails);
                          Navigator.pop(context);
                          _textController.clear();
                        }else{
                          _textController.clear();
                        }                        
                      },
               ), 
      actions: <Widget>[   
          RaisedButton(
            child: Text('CANCEL',style: TextStyle(color: Colors.black,fontSize: 15.0, fontFamily: 'Fredoka')),
            color: Colors.purple[100],
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(50.0)),
            onPressed:(){
              Navigator.pop(context);
              }
          ),
         RaisedButton(             //Done Button
          child: Text('DONE',style: TextStyle(color: Colors.black,fontSize: 15.0, fontFamily: 'Fredoka')),
          color: Colors.purple[100],
           shape: RoundedRectangleBorder(
             borderRadius: new BorderRadius.circular(50.0)),
          onPressed:(){          
            final v= _newEmail.contains('@gmail.com');   
                  if (v== true) {
                    print('Emal Button');
                    print(_newEmail);
                    sendRecognitionMail(_newEmail); //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                 _insert(_newEmail);
                 _saveMails();
                 print("mails after insert");
                          print(_mails);
                 Navigator.pop(context);
                 _textController.clear();
                  }
                  else{
                    _textController.clear();
                  }}
        ),
      ]
    )
    );
}

/*===================Remove Items from Animated List========================== */
 _removeItem(int i){
  String removed=_mails.removeAt(i);

 // AnimatedList.of(context).removeItem(i, (contexxt, animation) => _buildItem(removed,animation ,i));
  AnimatedListRemovedItemBuilder deleting=(context,animation){
    return _buildItem(removed,animation ,i); //use alone with delete listview 
    
  };
  _key.currentState.removeItem(i, deleting);
  }

/* ============History of Seizures============== */
_history(BuildContext context){
   if(globals.messageDate.length==0){
     return    Stack(    //Very Important ,It allow list with Button
        overflow: Overflow.visible,
        children: <Widget>[ 
        Container(
          alignment: Alignment.center,
          child:Text('No History',
            style: TextStyle(color: Colors.black,fontSize: 35.0, fontFamily: 'Fredoka') ,))
        ]);
    }else{
   return   Stack(    //Very Important ,It allow list with Button
  overflow: Overflow.visible,
  children: <Widget>[
  AnimatedList(        //Use Animated List to easily add and delete items
    padding: EdgeInsets.only(top: 20,right: 20, left:20,bottom: 20),
             initialItemCount: globals.messageDate.length,
                  key: _key,
                  itemBuilder: ( context,  index , animation){
                   return _buildHistory(globals.messageDate[index],animation,index);
                  }
                ),
        ]);
    }
    }
 _buildHistory(String items, Animation animation ,int index) {
  return SizeTransition(
   sizeFactor:animation,
    child: Container(        // Actual widget to display
      height: 80.0,
      width: 0.9*(MediaQuery.of(context).size.width),
      child: Card(
        margin: EdgeInsets.all(8),
        elevation: 1.0,
        color: Colors.deepPurple[100],
        shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(30.0),),
        child: Center(
          child: ListTile(
            contentPadding: EdgeInsets.only(left:35),
            title:Text(items,style: TextStyle(color: Colors.black,fontSize: 15.0, fontFamily: 'Fredoka'),),
          ),  
      ),
    ),
  )
  );
  }
}

