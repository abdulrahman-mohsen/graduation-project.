import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'PatientProfileScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'main.dart';
import 'RelativeProfileScreen.dart';

import 'globals.dart' as globals;

List<String> emptyHistory=[];

  final GoogleSignIn _googleSignIn =new GoogleSignIn(
   scopes: [
    'email',
      'https://mail.google.com/',
    ],
  );

  _signPatient(BuildContext context) async{   
    globals.loop=true; 
    print('Patient Sign IN');
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    final uid  = googleUser.id;
    print('ID: $uid');
    final GoogleSignInAuthentication googleAuth = await googleUser.authentication; 
    final accesstoken = googleAuth.accessToken; //toke in reem code
    final idtoken = googleAuth.idToken;
       print('Sign IN');
       print(googleUser.id);
       print(accesstoken);
       print(idtoken);
       print('DONE!!');

    SignDetails newDetails = new SignDetails( 
      googleUser.displayName,googleUser.photoUrl,googleUser.email,googleUser.id , accesstoken, 'x'
      );
      List<String> emptyMails=[];

      newDetails.uType = 'Patient';
     SharedPreferences prefs = await SharedPreferences.getInstance();
       prefs.setString('email', newDetails.uEmail);
       prefs.setString('name',newDetails.uName);  
       prefs.setString('photo',newDetails.uPhoto); 
       prefs.setString('id',newDetails.uID); 
       prefs.setString('accesstook',newDetails.took); 
       prefs.setString('type', newDetails.uType); 
       prefs.setStringList( 'R_emails', []);      
       //prefs.setStringList('History',[]);

    Navigator.push(context,
       MaterialPageRoute(
       //builder: (context)=> new NewProfileScreen(newDetails: details),
       builder: (context)=> new PatientProfileScreen (details: newDetails,mails:emptyMails,history:emptyHistory),
      ) ,
    );
  }

  _signRelative(BuildContext context) async{ 
    globals.loop=true;   
    print('Relative Sign IN');
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    final uid  = googleUser.id;
    print('ID: $uid');
    final GoogleSignInAuthentication googleAuth = await googleUser.authentication; 
    final accesstoken = googleAuth.accessToken; //toke in reem code
    final idtoken = googleAuth.idToken;
       print('Sign IN');
       print(googleUser.id);
       print(accesstoken);
       print(idtoken);
       print('DONE!!');
       
    SignDetails newDetails = new SignDetails( 
      googleUser.displayName,googleUser.photoUrl,googleUser.email,googleUser.id , accesstoken, 'x');

     newDetails.uType = 'Relative';
     SharedPreferences prefs = await SharedPreferences.getInstance();
       prefs.setString('email', newDetails.uEmail);
       prefs.setString('name',newDetails.uName);  
       prefs.setString('photo',newDetails.uPhoto); 
       prefs.setString('id',newDetails.uID); 
       prefs.setString('accesstook',newDetails.took); 
       prefs.setString('type', newDetails.uType);
      // prefs.setStringList( 'R_emails', []); 
       //prefs.setStringList('History',[]);     
       
    Navigator.push(context,
      new MaterialPageRoute(
       //builder: (context)=> new NewProfileScreen(newDetails: details),
       builder: (context)=> new RelativeProfileScreen(details: newDetails,history: emptyHistory),
      ) ,
    );
  }


class SignIN extends StatelessWidget {

  @override
  Widget build(BuildContext context) {        
    return new MaterialApp(
      title: 'Close Flutter',
      debugShowCheckedModeBanner: false,
      home: Scaffold(
      backgroundColor: Colors.brown[50],
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
             Text(
                "Sign In As:",
                style:  TextStyle(fontWeight: FontWeight.bold, color: Colors.black54,fontSize: 40.0,fontFamily: 'Fredoka'),
              ),
           SizedBox(height:50.0),
            SizedBox(
              width: 200,
              height: 70,
              child:RaisedButton(
                color: Colors.deepPurple[200], ////why don'y work??????
                shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(50.0)),
                  child:Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                    Icon(FontAwesomeIcons.brain, color: Colors.pink,size: 27,),
                    Text( 'Patient',
                      style: TextStyle(color: Colors.black54,fontSize: 25.0,fontFamily: 'Fredoka'),)
                     ],),
               onPressed: () {
                 _signPatient(context);
               }
            ),),
           SizedBox(height:30.0),
             SizedBox(
              width: 200,
              height: 70,
              child:RaisedButton(
                color: Colors.teal[100], ////why don'y work??????
                shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(50.0)),
                  child:Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                    Icon(FontAwesomeIcons.userFriends, color: Colors.pink,size: 27,),
                    Text( ' Relative',
                      style: TextStyle(color: Colors.black54,fontSize: 25.0,fontFamily: 'Fredoka')
                  )],),
               onPressed: () {
                 _signRelative(context);
               } 
            ))
          ],
        )
      )
    )
    );
  }
}