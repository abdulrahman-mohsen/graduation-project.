import 'dart:async';
import 'package:flutter/material.dart';

import 'signin.dart';
import 'PatientProfileScreen.dart';
import 'RelativeProfileScreen.dart';


import 'package:shared_preferences/shared_preferences.dart';


void main() => runApp(MyApp());


Future <void> check(context) async{
   SharedPreferences prefs = await SharedPreferences.getInstance();
   String ifuser = prefs.getString('email');
   print(ifuser);
   if(ifuser != null){
      print('current');
      _current(context);
    }
    else
    {
      print('new');
      _signIn(context);
    }  
}

Future<void>_current(context) async{   
  SharedPreferences prefs = await SharedPreferences.getInstance();
   String email  =  prefs.getString('email');
   String name   =  prefs.getString('name');  
   String photo  =  prefs.getString('photo'); 
   String id     =  prefs.getString('id'); 
   String took   =  prefs.getString('accesstook'); 
   String type   =  prefs.getString('type'); 
   List<String>  rMails  =  prefs.getStringList('R_emails');
   List history  =  prefs.getStringList('History');

  SignDetails olddetails = new SignDetails( 
      name , photo,email,id,took, type 
      //,rMails,history
  ); 

  if (type == 'Patient'){
    print('current Patient');
     Navigator.push(context,
     MaterialPageRoute(
      builder: (context)=>  PatientProfileScreen(details: olddetails,mails:rMails,history:history),
    ) ,
  ); 
  }
  else if (type =='Relative'){
    print('current Relative');
    Navigator.push(context,
     MaterialPageRoute(
      builder: (context)=>  RelativeProfileScreen(details: olddetails,history:history),
    ) ,
  ); 
  }
}     

 _signIn(context) {     
   Navigator.push(context,
     MaterialPageRoute(
       builder: (context)=>  SignIN()),
    ); 
  }

  

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(  
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}


class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  _MyHomePageState();

  
  @override
  
  Widget build(BuildContext context) {
  Timer(Duration(seconds: 5), () {
      check(context);
  });

    return Scaffold(
      body: Builder(
        builder: (context) => Stack(
       fit: StackFit.expand,
        children: <Widget>[
            Container(
             foregroundDecoration: const BoxDecoration(
             image: DecorationImage(
             image: AssetImage(
            'assets/brain.jpg'),
             fit: BoxFit.cover,
             colorFilter: ColorFilter.mode(Color.fromRGBO(255, 255, 255, 0.6), BlendMode.modulate)
             ),
              ),),
             
             //child:Image.asset( "assets/brain.jpg",
             //fit: BoxFit.contain,
             //color: Color.fromRGBO(255, 255, 255, 0.6),
            // colorBlendMode: BlendMode.modulate,)
            
           // Container(
              //height: 250.0,
              //child:
              
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly ,
                children:<Widget>[
                  SizedBox(height: 350),
                     Container(
              //alignment: Alignment.center,
              child: Text( 'WELCOME',
                      style: TextStyle(
                        fontFamily: 'Fredoka' ,
                        color: Colors.pinkAccent,fontSize: 65.0),)
              ),
              
                ]
              
            ),
            
             ],
        
        )
        )
    );
  }
}

class SignDetails {
   String uName;
   String uPhoto;
   String uEmail;
   String uID;
   String took;
   String uType;
  // List<String> mails;
   //List<String> history;
  
 
 SignDetails( this.uName,this.uPhoto,this.uEmail,this.uID,this.took, this.uType
 //,this.mails,this.history
 );
}
